# Lab 2: Satellite for Content Host Management

<!-- TOC -->

- [Lab 2: Satellite for Content Host Management](#lab-2-satellite-for-content-host-management)
	- [Create Activation Key:](#create-activation-key)
	- [Register Content Hosts](#register-content-hosts)
	- [Update Content Hosts:](#update-content-hosts)

<!-- /TOC -->

The preconfigured “rhel7” activation key is required for subsequent lab
functions. Do not remove this activation key.

### Create Activation Key:

1.  Navigate to Activation Keys

	* To Navigate to the Activation Keys page, select “Content” from your toolbar, and select “Activation Keys” from the dropdown.

2.  Create Activation Key

	* From the Activation Keys page, click the blue “Create Activation Key” button in the top right corner
	* Name Activation Key whatever you’d like, and associate with your new Lifecycle Environment and Content View from the previous lab.

    **NOTE:** We have pre-built a Lifecycle Environment and Content View in case you did not do Lab 1. If that’s the case, feel free to use these pre-built objects going forward.

	* Click “Save” to save your Activation Key.

![](images/image28.jpg)

3.  Attach Subscriptions to Activation Key

	*  From the Activation Keys page, select your newly created activation key.
	*  Select “Subscriptions” from the Activation Key toolbar

	**NOTE:** If continuing from the previous step, this may direct you to this page automatically.

	* Select “Add” from the new section available.
	* Click the checkbox next to the “Employee SKU” and then click “Add Selected” in the top right corner of the this new section.  

![](images/image100.jpg)

4.  Configure “Enabled Repository” Default

	* From the Activation Keys page, select your newly created Activation Key
	* Select “Repository Sets” from the Activation Key toolbar

	**NOTE:** If continuing from the last step, this may direct you to this page automatically.
	**NOTE:** the repositories may take a moment to load due to using the Employee SKU in this example.

	* Click the checkbox next to each of the following repositories

	```
	Red Hat CloudForms Management Engine 5.8 (RPMs)
	Red Hat Enterprise Linux 7 Server - Extras (RPMs)
	Red Hat Enterprise Linux 7 Server (Kickstart)
	Red Hat Enterprise Linux 7 Server - RH Common (RPMs)
	Red Hat Enterprise Linux 7 Server (RPMs)
	Red Hat Enterprise Linux 7 Server - Supplementary (RPMs)
	Red Hat Software Collections RPMs for Red Hat Enterprise Linux 7Server
	Red Hat Satellite Tools 6.3 (for RHEL 7 Server) (RPMs)
	```

	* Click “Select Action” followed by “Override to Enabled” in the top right corner of the Repository Selection section.

![](images/image7.jpg)

**Background:**

By creating an Activation Key, and associating the created Lifecycle Environments, Content Views, Subscriptions, and Repositories, we’ve enable the ability to activate simple, precise, and efficient deployment of new systems.

Activation Keys can be used to easily automate provisioning, as well as allow for consistent registration and initialization of Content Hosts without requiring users to obtain admin credentials.

### Register Content Hosts

1.  Navigate to Content Hosts

	* To Navigate to the Content Hosts page, select “Hosts” from your toolbar, and select “Content Hosts” from the dropdown.

![](images/image41.jpg)

2.  Register Content Host

	* Click “Register Content Host” button in the top right corner
	* Follow steps outlined in the Satellite WebUI, on each Content Host (See “Background” section for client access steps).

		* Select Content Source (this will be your desired Satellite or Capsule server). Based on your selection here, it will adjust the following rpm location accordingly.
		* Install pre-built bootstrap RPM. This adjusts configurations on the host to point to your satellite/capsule for registration, subscription, and content delivery.
		* From the client console, run subscription-manager using the Activation Key created in Step 1.
		* Ensure Satellite Tools repository is enabled (If Activate Key was pre-configured to enable this repo by default, this should be set in the last step). This provides access to Katello Agent in the next step.
		* Install Katello Agent, which provides the ability to run remote execution(like content patching), as well as displays the errata status (applicable bugs, security, etc) for each Content Host.

	* Repeat for client systems ic[2-4].example.com (See “Background” section on next page for client access steps).

![](images/image99.jpg)

        

**Background:**

In order to accomplish this, you’ll need to login to your client VM, first SSH into your workstation node at
workstation-\<GUID\>.rhpds.opentlc.com as lab-user, the sudo to root. A ssh key is already in the home directory of your laptop, which should allow you to login without a password. Should a password be required,
use “r3dh4t1!”  as  your password.

```
[lab-user@localhost \~]\$ ssh lab-user@workstation-GUID.rhpds.opentlc.com
[lab-user@workstation-GUID \~]\$ sudo -i\
[root@workstation \~]\#
```


From there, this is your “jumpbox” that will allow you to access each of the client machines (ic1.example.com through ic4.example.com), also using ssh (ssh passwordless has already been configured for your convenience) and repeat the following commands in ic2-ic4 machines(ic1.example.com has been configured for you):

**IMPORTANT:** Only Do this for all ic1-ic4 machines, ic5-ic9 are available, but will be used for a subsequent lab.

In this scenario we are taking over management of these systems from another team, that may have previously had them registered to a different, or older Satellite server.

```
[root@workstation \~]\# ssh ic1.example.com

Last login: Mon Apr  9 07:01:12 2018 from tower.example.com

[root@ic1 \~]\# yum clean all ;  rm -rf /var/cache/yum/\*

[root@ic1 \~]\# rpm -Uvh http://sat.example.com/pub/katello-ca-consumer-latest.noarch.rpm

[root@ic1 \~]\# subscription-manager register --org="Default\_Organization" --activationkey="rhel7"

[root@ic1 \~]\# subscription-manager repos --enable=rhel-\\\*-satellite-tools\\\*-rpms

[root@ic1 \~]\# yum -y install katello-agent

[root@ic1 \~]\# systemctl restart goferd
```

![](images/image42.jpg)

**Tip:** Here are all commands on individual lines -- so you can you know….copy and paste. :)

```
yum clean all ;  rm -rf /var/cache/yum/\* \\

rpm -Uvh [http://sat.example.com/pub/katello-ca-consumer-latest.noarch.rpm](https://www.google.com/url?q=http://sat.example.com/pub/katello-ca-consumer-latest.noarch.rpm&sa=D&ust=1554754711356000) \\

subscription-manager register --org="Default\_Organization" --activationkey="rhel7" \\

subscription-manager repos --enable=rhel-\\\*-satellite-tools\\\*-rpms \\

yum -y install katello-agent \\

systemctl restart goferd
```

Remember to do the above commands on hosts ic1 through ic4.

```

[root@workstation \~]\# ssh ic2.example.com

yum clean all ;  rm -rf /var/cache/yum/\*

rpm -Uvh http://sat.example.com/pub/katello-ca-consumer-latest.noarch.rpm

subscription-manager register --org="Default\_Organization" --activationkey="rhel7"

subscription-manager repos --enable=rhel-\\\*-satellite-tools\\\*-rpms

yum -y install katello-agent

systemctl restart goferd

[root@workstation \~]\# ssh ic3.example.com

yum clean all ;  rm -rf /var/cache/yum/\*

rpm -Uvh http://sat.example.com/pub/katello-ca-consumer-latest.noarch.rpm

subscription-manager register --org="Default\_Organization" --activationkey="rhel7"

subscription-manager repos --enable=rhel-\\\*-satellite-tools\\\*-rpms

yum -y install katello-agent

systemctl restart goferd

[root@workstation \~]\# ssh ic4.example.com

yum clean all ;  rm -rf /var/cache/yum/\*

rpm -Uvh http://sat.example.com/pub/katello-ca-consumer-latest.noarch.rpm

subscription-manager register --org="Default\_Organization" --activationkey="rhel7"

subscription-manager repos --enable=rhel-\\\*-satellite-tools\\\*-rpms

yum -y install katello-agent

systemctl restart goferd

```

### Update Content Hosts:

1.  Navigate to Content Host

	* To Navigate to the Content Hosts page, select “Hosts” from your toolbar, and select “Content Hosts” from the dropdown.
	* Notice after registering with the previously configured activation key, we now have the newly registered Content Host reporting it’s subscription status, Lifecycle Environment and Content View associations, and Installable Errata.
	* Some of the hosts may have the .localdomain domain instead of .example.com domain. This is a limitation of the lab     environment’s networking, but all actions should complete successfully against these hosts in the environments.

![](images/image1.jpg)

2.  Navigate to Content Host’s Installable Errata

	* From the Content Hosts page, select a newly registered Content Host, for example ic1.example.com

	**NOTE:** If continuing from the last step, this should direct you to this page automatically.

	* This will bring up the Details page by default. From here, select “Errata” from the Content Hosts toolbar.

	![](images/image96.jpg)

	![](images/image54.png)

3.  Update Content Host

	* Looking at the Errata for this system, it’s clear the system is severely out of date. Given this, we have a couple of options to update the clients (Specifically Push/Pull).

		* Pull: Probably the most widely recognized method, and sometimes faster than selecting hundreds of errata from Satellite UI, from the client system, we can simply run “yum update” from the client console after registering and subscribing:

			* SSH from your jumpbox to the affected host, in this case

    		```
    		ic1.example.com: \# ssh root@ic1.example.com
			[root@ic1 \~]\# yum -y update
			```

		* Push: We can push specific errata from the satellite, to the client by selecting the specific desired errata from this list, and clicking “Apply Selected”.

			**NOTE:** For a number of outdated systems, the following packages may need to be updated and goferd needs to be restarted in order to successfully update via push from the  Satellite 6.3 host. These packages have already been updated in your environment, but the goferd service will likely still need to be restarted.

			* SSH from your jumpbox to the affected host, in this case

			```
    		ic1.example.com: \# ssh root@ic1.example.com

			[root@ic1 \~]\# yum -y upgrade katello-agent \\

			katello-host-tools katello-host-tools-fact-plugin \\

			pulp-rpm-handlers qpid-proton-c

			[root@ic1 \~]\# systemctl restart goferd
			```

			**NOTE:** If above issue is encountered, it will be evident by either the error “No handler for: {‘type’: u’erratum’}”, or a communication timeout.

	* To Install All Errata you can select to view up to 100 available errata at a time at the bottom of the screen, and select all from the master checkbox in the top left corner of the view, then Apply Selected.

	* Repeat this step until all errata have been applied.
	* For systems registered to a prior Satellite, like this scenario: there may be some old remnants of the previous system registration. In this case, the remote push errata update may fail. You can login to the system and as root run the commands:

			# yum clean all ;  rm -rf /var/cache/yum/*

		* This clears out the old yum repo data from the previous satellite
    registration, and prepares the system for updates from your newly
    configured Satellite server.

![](images/image113.jpg)

* Once the errata has been applied you can verify there’s no pending
    erratas on the UI.

![](images/image84.png)




Continue to the next step:[Lab 3: Proactive Security and Automated Risk Management with Red Hat Insights](../lab3-insights/index.md)
