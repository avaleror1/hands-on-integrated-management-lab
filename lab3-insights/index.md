# Lab 3: Proactive Security and Automated Risk Management with Red Hat Insights

<!-- TOC -->

- [Lab 3: Proactive Security and Automated Risk Management with Red Hat Insights](#lab-3%C2%A0proactive-security-and-automated-risk-management-with-red-hat-insights)
  - [Goal of Lab](#goal-of-lab)
  - [Introduction](#introduction)
  - [Fixing the payload injection security issue in your system using Red Hat Insights from the Satellite UI](#fixing-the-payload-injection-security-issue-in-your-system-using-red-hat-insights-from-the-satellite-ui)
    - [Cleaning your environment](#cleaning-your-environment)
    - [Adding your Insights hosts to Satellite](#adding-your-insights-hosts-to-satellite)
    - [Installing the Insights client](#installing-the-insights-client)
    - [Fixing the payload injection security issue](#fixing-the-payload-injection-security-issue)
    - [Automatically fixing the payload injection security issue via Ansible Playbook](#automatically-fixing-the-payload-injection-security-issue-via-ansible-playbook)
    - [Bonus! Automatically fix all the issues on systems ic8.example.com and ic9.example.com that have playbooks](#bonus-automatically-fix-all-the-issues-on-systems-ic8examplecom-and-ic9examplecom-that-have-playbooks)

<!-- /TOC -->

## Goal of Lab


The goal of this lab is to introduce you to the proactive security
capabilities of Red Hat Insights. This lab assumes the following:

-   You started all your VMs, per the instructions in Lab 0.
-   Did not delete any of the key items in the pre-populated Satellite 6
    configuration, which are necessary for Insights to work properly.

## Introduction


Red Hat Insights was designed to proactively evaluate the security,
performance, and stability of your Red Hat platforms by providing
prescriptive analytics of your systems. Insights helps move you from
reactive to proactive systems management, delivers actionable
intelligence, and increases visibility of infrastructure risks and the
latest security threats. Operational analytics from Insights empowers
you to prevent downtime and avoid firefighting, while also responding
faster to new risks.

In this lab, we will focus only on the specific security features of Red
Hat Insights.

Red Hat Insights recommendations are tailored for the individual system
where risk is detected. This allows you to be certain that actions
identified by Insights are validated and have a verified resolution for
each detected risk, reducing false positives you may experience from
critical risks identified by third-party security scanners. Insights
provides predictive analysis of security risk in your infrastructure
based on a constantly evolving threat feed from Red Hat.

Through analysis of Insights metadata and curated knowledge based on
over fifteen years of enterprise customer support, Red Hat is able to
identify critical security vulnerabilities, statistically frequented
risks, and known bad configurations. We scale this knowledge to our
customers with Insights reporting and alerts, allowing prediction of
what will happen on a monitored system, why it will happen, and how to
fix a problem before it can occur.

Red Hat Insights functionality is integrated into Red Hat’s Customer
Portal, Satellite, CloudForms, and Ansible Tower by Red Hat.
 Recommendations from Insights are human-readable and in most cases can
simply be copy and pasted into the terminal to resolve the issue. You
may also automate remediation of hosts in your infrastructure with
Insights generated Ansible playbooks or Ansible Tower integration.

## Fixing the payload injection security issue in your system using Red Hat Insights from the Satellite UI

### Cleaning your environment

As you may have discerned from the previous lab exercises, you’ve been
put in charge of some  new-to-you servers. These servers were previously
registered to an old Satellite server. You’ll need to re-register them
to your company’s new Satellite, using the provided bootstrap script to
quickly and easily begin managing them, and fix some of the critical
vulnerabilities with these systems. In a real-world-scenario, Insights
can be installed automatically by Satellite upon provisioning or
registration of a new system to the Satellite, which means you do not
have to manually login and register each host. These steps can be easily
automated with Ansible or a shell script, but for sake of completion we
will perform them manually in these labs with only a few systems.

To login to your client VM, first SSH into your workstation node at
workstation-\<GUID\>.rhpds.opentlc.com as lab-user. An ssh key is
already in the home directory of your laptop, which should allow you to
login without a password. Should a password be required, use “r3dh4t1!”
 as  your password.
```
[lab-user@localhost \~]\$ ssh workstation-GUID.rhpds.opentlc.com

[lab-user@workstation-GUID \~]\$ sudo -i\

[root@workstation \~]\#
```

From it, jump into every single one of the insights client machines
(from ic5.example.com to ic9.example.com), also using ssh (ssh
passwordless has already been configured for your convenience):

`[root@workstation \~]\# ssh ic5.example.com`


Make sure old satellite info is removed:

```
[root@ic5 \~]\# yum clean all ; rm -rf /var/cache/yum/\*

[root@ic5 \~]\# subscription-manager clean

[root@ic5 \~]\# rm -fv /etc/rhsm/ca/katello\*

[root@ic5 \~]\# rm -fv /etc/rhsm/facts/katello\*

[root@ic5 \~]\# rm -rfv /var/lib/puppet

[root@ic5 \~]\# yum remove -y katello-ca-consumer\*
```

### Adding your Insights hosts to Satellite

Pull down new bootstrap script from new Satellite 6 server:

```

[root@ic5 \~]\# curl https://sat.example.com/pub/bootstrap.py \>bootstrap.py --insecure

[root@ic5 \~]\# chmod +x bootstrap.py
```

Run the bootstrap.py script using the Satellite manifest that uses
certificate based authorization in order to register your machines with
Satellite as follows (provide the password when required):

```
# ./bootstrap.py -l admin -s sat.example.com -o 'EXAMPLE.COM' -L 'Default Location' -g rhel7 -a rhel7

Foreman Bootstrap Script

This script is designed to register new systems or to migrate an
existing system to a Foreman server with Katello

admin's password:

[...]

Loaded plugins: product-id, subscription-manager

[SUCCESS], [2019-04-8 12:44:33], [/usr/bin/yum -y remove rhn-setup
rhn-client-tools yum-rhn-plugin rhnsd rhn-check rhnlib spacewalk-abrt
spacewalk-oscap osad 'rh-\*-rhui-client' 'candlepin-cert-consumer-\*'],
completed successfully.
```

**IMPORTANT!*** Do not forget to repeat the steps for:

```
On your workstation \$ sudo -i then

# ssh ic6.example.com
yum clean all ; rm -rf /var/cache/yum/\*
subscription-manager clean
rm -fv /etc/rhsm/ca/katello\*
rm -fv /etc/rhsm/facts/katello\*
rm -rfv /var/lib/puppet
yum remove -y katello-ca-consumer\*
curl https://sat.example.com/pub/bootstrap.py \> bootstrap.py --insecure
chmod +x bootstrap.py
./bootstrap.py -l admin -s sat.example.com -o 'EXAMPLE.COM' -L 'Default Location' -g rhel7 -a rhel7

# ssh ic7.example.com
yum clean all ; rm -rf /var/cache/yum/\*
subscription-manager clean
rm -fv /etc/rhsm/ca/katello\*
rm -fv /etc/rhsm/facts/katello\*
rm -rfv /var/lib/puppet
yum remove -y katello-ca-consumer\*
curl https://sat.example.com/pub/bootstrap.py \> bootstrap.py --insecure
chmod +x bootstrap.py
./bootstrap.py -l admin -s sat.example.com -o 'EXAMPLE.COM' -L 'Default Location' -g rhel7 -a rhel7

# ssh ic8.example.com
yum clean all ; rm -rf /var/cache/yum/\*
subscription-manager clean
rm -fv /etc/rhsm/ca/katello\*
rm -fv /etc/rhsm/facts/katello\*
rm -rfv /var/lib/puppet
yum remove -y katello-ca-consumer\*
curl https://sat.example.com/pub/bootstrap.py \> bootstrap.py --insecure
chmod +x bootstrap.py
./bootstrap.py -l admin -s sat.example.com -o 'EXAMPLE.COM' -L 'Default Location' -g rhel7 -a rhel7

# ssh ic9.example.com
yum clean all ; rm -rf /var/cache/yum/\*
subscription-manager clean
rm -fv /etc/rhsm/ca/katello\*
rm -fv /etc/rhsm/facts/katello\*
rm -rfv /var/lib/puppet
yum remove -y katello-ca-consumer\*
curl https://sat.example.com/pub/bootstrap.py \> bootstrap.py --insecure
chmod +x bootstrap.py
./bootstrap.py -l admin -s sat.example.com -o 'EXAMPLE.COM' -L 'Default Location' -g rhel7 -a rhel7
```

And repeat the steps. If no errors, proceed to the next section, If
there was an error see below:

**NOTE:** If bootstrap.py fails with this error:

```

[RUNNING], [2019-04-8 06:28:43], [Calling Foreman API to create a host
entry associated with the group & org]

An error occurred: HTTP Error 422: Unprocessable Entity

url: https://sat.example.com:443/api/v2/hosts/

code: 422

[...]

    "full\_messages": [

      "Name has already been taken"

[...]

Output truncated
```

then it means machines have already been registered using subscription
manager, in that case, you just have to delete the hosts from the
Satellite UI and re-run bootstrap. See the below graphics. If your hosts
were successful, continue past these screenshots.

In your Firefox web browser, click on the tab you have opened to your
Red Hat Satellite UI. Log back in with admin as the username and
r3dh4t1! as your password.

Go to Hosts → All Hosts and you will see them listed as follows:

![](images/image11.png)

Select the ic[1-4].example.com hosts and delete them by going to Select
Action → Delete Hosts, as in the following screen.

![](images/image64.png)

Then, run the bootstrap.py script again.

When logging into the Satellite UI you should see your systems
registered. Go to Hosts → Content Hosts and you will see them listed as
follows:

![](images/image79.png)

### Installing the Insights client

Now it’s the time to install the Insights RPM and register your system
to Red Hat Insights.

bootstrap.py should have taken care of this for you IF your lab is setup
natively to the environment, if you changed some settings this may not
happen automatically. 

If the systems are not showing up as registered, proceed with installing
the RPMs on each host.

NOTE: On RHEL 7.5 client RPM has been renamed to insights-client, but
this laboratory machines are using RHEL 7.0 and 7.3 for demonstration
purposes, so the package name is still the old one.

To install Insights RPM in each of your systems issue the following
command:

`[root@ic5 \~]\# yum -y install redhat-access-insights`

And then, simply register each machine with Red Hat Insights as follows:

```
[root@ic5 \~]\# redhat-access-insights --register

Automatic daily scheduling for Insights has been enabled.

Starting to collect Insights data

Uploading Insights data, this may take a few minutes

Upload completed successfully!
```

### Fixing the payload injection security issue

Now, going back to the Satellite UI, click on Red Hat Insights →
Overview, where you should see all your registered systems, actions
summary (highlighted by priority) as well as latest updates from Red
Hat.

 

![](images/image53.png)

In this lab, we will fix the specific “Kernel vulnerable to
man-in-the-middle via payload injection (CVE-2016-5696)” on your client
VMs without causing downtime.

STEPS:

1.  From your Satellite  UI, click on Red Hat Insights → Inventory.

        ![](images/image69.png)

2.  Click on your client VM, which is ic6.example.com. You will see the
    list of issues affecting it when clicking on the system name.

![](images/image59.png)

3.  Notice that your system shows up with multiple security
    vulnerabilities.

**Note:** Our objective is to fix the payload injection problem without
causing downtime, and see that it no longer appears as a vulnerability
in Insights. Specifically, this payload injection problem causes the
kernel to be vulnerable to man-in-the-middle via payload injection. A
flaw was found in the implementation of the Linux kernel's handling of
networking challenge ack ([RFC
5961](https://www.google.com/url?q=https://tools.ietf.org/html/rfc5961&sa=D&ust=1554754711389000))
where an attacker is able to determine the shared counter. This flaw
allows an attacker located on different subnet to inject or take over a
TCP connection between a server and client without needing to use a
traditional man-in-the-middle (MITM) attack.

4.  Use your browser’s search function to search for “payload
    injection”.

Note: Reading the description for the vulnerability shows that the
sysctl variable is set to a level that allows being exploited. We want
to do the active mitigation by changing the sysctl variable and making
it permanent on reboot. In this case, we do not want to update the
kernel or reboot since we don’t want downtime.

![](images/image45.png)

5.  If not already there, login to your client VM, first SSH into your
    workstation node at workstation-\<GUID\>.rhpds.opentlc.com as
    lab-user. An ssh key is already in the home directory of your
    laptop, which should allow you to login without a password. Should a
    password be required, use “r3dh4t1!”  as  your password.

```

[lab-user@localhost \~]\$ ssh workstation-GUID.rhpds.opentlc.com

[lab-user@workstation-GUID \~]\$ sudo -i\
[root@workstation \~]\#
```

6.  Now that you are in the workstation node, SSH into your RHEL7
    client/host.

`# ssh ic6`

7.  Now, as root, perform the recommended active mitigation. Edit the
    /etc/sysctl.conf file to add the mitigation configuration, and
    reload the kernel configuration:

```
# echo "net.ipv4.tcp\_challenge\_ack\_limit = 2147483647" >>/etc/sysctl.conf

# sysctl -p

net.ipv4.tcp\_challenge\_ack\_limit = 100

vm.legacy\_va\_layout = 0

net.ipv4.tcp\_challenge\_ack\_limit = 2147483647
```

8.  After applying the active mitigation, we want to have the system
    report any changes, run the following command as root on
    ic6.example.com: 

```

# redhat-access-insights

Starting to collect Insights data

Uploading Insights data, this may take a few minutes

Upload completed successfully!
```

Wait until this step completes before moving to the next step.

9. From your Satellite  UI, click on Red Hat Insights → Inventory.

        ![](images/image69.png)

10. Click on your client VM, which is ic6.example.com. You will notice
    than the number of actions has decreased (from 18 to 17).

        ![](images/image98.png)

11. Use your browser’s search function to search for “payload
    injection”. You will notice that this payload injection issue is no
    longer listed due to fixing the vulnerability.

![](images/image25.png)

Congratulations, you’re no longer vulnerable to the payload injection
vulnerability!

### Automatically fixing the payload injection security issue via Ansible Playbook

It is also possible to automate some of the issues with an Ansible
Playbook that Insights provides us. You can see that in the top left
corner of every single issue with the Ansible logo in blue if a playbook
is available, or in grey if it’s not.

![](images/image103.png)

In the particular case of the payload injection security issue, an
Ansible Playbook is available for us.

![](images/image109.png)

Now we need to create a plan in which the issues that are found will be
solved using an Ansible Playbook. In order to do so, from your Satellite
 UI, click on Red Hat Insights → Planner.

![](images/image88.png)

And once there, click on Create a plan.

![](images/image20.png)

Fill in the boxes as in the example, and do not forget to select only
the payload injection security issue and select ic7.example.com as the
system in which this solution is to be applied. Then click “save”.

  

![](images/image85.png)

As seen in the previous part of this laboratory, there are two ways to
solve this issue, one is by updating the kernel, and the other one is
apply the needed changes to the /etc/sysctl.conf file to add the
mitigation configuration, and reload the kernel configuration.

Insights gives us the opportunity to choose the resolution that we want.
Please make sure to select “Set sysctl ip4 challenge ack limit” as your
preferred choice and then click on the Save button.

 ![](images/image92.png)

Once the plan is saved, the planner screen is shown where you can see
the newly created plan, as well as the issues it resolves and the
systems affected.

![](images/image67.png)

You should now download the playbook, however, it’s been already
downloaded for your convenience to the tower machine.

If not already there, login to your client VM, first SSH into your
workstation node at workstation-\<GUID\>.rhpds.opentlc.com as the
lab-user. An ssh key is already in the home directory of your laptop,
which should allow you to login without a password. Should a password be
required, use “r3dh4t1!”  as  your password.

```

[lab-user@localhost \~]\$ ssh workstation-GUID.rhpds.opentlc.com

[lab-user@workstation-GUID \~]\$ sudo -i\
[root@workstation \~]\#
```

Now that you are in the workstation node, SSH into the tower machine in
order perform the recommended active mitigation with the Ansible
Playbook. 

```
[root@workstation \~]\# ssh tower
```

Inspect the Ansible Playbook that Insights has created automatically for
you:

```
[root@tower \~]\# less payload-injection.yml

---

# Red Hat Insights has recommended one or more actions for you, a system administrator, to review and if you
# deem appropriate, deploy on your systems running Red Hat software. Based on the analysis, we have automatically
# generated an Ansible Playbook for you. Please review and test the recommended actions and the Playbook as
# they may contain configuration changes, updates, reboots and/or other changes to your systems. Red Hat is not
# responsible for any adverse outcomes related to these recommendations or Playbooks.
#
# Addresses maintenance plan 34918 (playload)
# https://access.redhat.com/insights/planner/34918
# Generated by Red Hat Insights on Tue, 9 Apr 2019 09:54:52 GMT
# Kernel vulnerable to man-in-the-middle via payload injection (CVE-2016-5696)
# Identifier: (CVE_2016_5696_kernel|KERNEL_CVE_2016_5696_URGENT,105,mitigate)
# Version: c988b9061f0c3720900ae391d72a59a89bf57294

- name: Set sysctl ipv4 challenge ack limit
  hosts: "ic7.example.com"
  become: true
  tasks:
        - name: "set the sysctl net.ipv4.tcp\_challenge\_ack\_limit = 2147483647"
          sysctl:
            name: net.ipv4.tcp\_challenge\_ack\_limit
            value: 2147483647
            sysctl_set: true
- name: run insights
  hosts: ic7.example.com
  become: True
  gather_facts: False
  tasks:
        - name: run insights
          command: redhat-access-insights
          changed_when: false
          
```

Now, simply proceed to remediate the payload injection security issue by
executing the Ansible Playbook as follows:

```

[root@tower \~]\# ansible-playbook payload-injection.yml

PLAY [Set sysctl ipv4 challenge ack limit]
************************************************************************

TASK [Gathering Facts]
************************************************************************

ok: [ic7.example.com]

TASK [set the sysctl net.ipv4.tcp\_challenge\_ack\_limit = 2147483647]
************************************************************************

changed: [ic7.example.com]

PLAY [run insights]
************************************************************************

TASK [run insights]
************************************************************************

ok: [ic7.example.com]

PLAY RECAP
************************************************************************

ic7.example.com                :
ok=3        changed=1        unreachable=0        failed=0

```

Please note that when the execution is completed, the Insights agent is
also run, so the latest state of the system is reporting into Insights
automatically.

Now from the Satellite UI, click on Red Hat Insights → Inventory you
will notice that system ic7.example.com has one less issue, just like
ic6.example.com (both with 17).

![](images/image120.png)

### Bonus! Automatically fix all the issues on systems ic8.example.com and ic9.example.com that have playbooks

From the Satellite UI, click on Red Hat Insights → Inventory so we can
focus on systems ic8.example.com and ic9.example.com, please notice
these two show 18 actions each to be solved.

![](images/image110.png)

In the inventory screen, select both systems and click on Actions, on
the top left corner, and then select Create a new Plan / Playbook.

![](images/image106.png)

This way, we are going to create an Ansible Playbook-based plan to solve
issues on those two specific systems (systems can also be grouped as per
our convenience, from that very same menu).

The Plan / Playbook Builder screens appears. Please make sure to fill
the boxes as follows:

Plan name: ic8-ic9-all

Actions: all (do this by clicking on the box by the Action label at the
top).

Your screen should look like:

![](images/image38.png)

Then click on the Save button in the bottom right corner.

As before, you are given the option to choose between different ways to
solve your issues. In this lab, we’ve chosen to go for the ones that do
not require a reboot, in order to save some time.

The plan description screen appears.

You should see all the issues this plan is going to solve as well as the
affected systems.

![](images/image52.png)

Scrolling down the screen, you should be able to download the playbook.
Per your convenience, this has already been downloaded to the tower
machine.

![](images/image102.png)

Like in the previous exercise, we need to log into the tower machine in
order to run the Ansible Playbook.

If not already there, login to your client VM, first SSH into your
workstation node at workstation-\<GUID\>.rhpds.opentlc.com as the
lab-user. An ssh key is already in the home directory of your laptop,
which should allow you to login without a password. Should a password be
required, use “r3dh4t1!”  as  your password.

```

[lab-user@localhost \~]\$ ssh workstation-GUID.rhpds.opentlc.com

[lab-user@workstation-GUID \~]\$ sudo -i\
[root@workstation \~]\#

```

Now that you are in the workstation node, SSH into the tower machine in
order perform the recommended active mitigation with the Ansible
Playbook. 

```
[root@workstation \~]\# ssh tower
```

Inspect the Ansible Playbook that Insights has created automatically for
you:
```

[root@tower \~]\# vi ic8-ic9-all.yml

---

# Red Hat Insights has recommended one or more actions for you, asystem administrator, to review and if you
# deem appropriate, deploy on your systems running Red Hat software. Based on the analysis, we have automatically
# generated an Ansible Playbook for you. Please review and test the recommended actions and the Playbook as
# they may contain configuration changes, updates, reboots and/or other changes to your systems. Red Hat is not
# responsible for any adverse outcomes related to these recommendations or Playbooks.
#
# Addresses maintenance plan 34921 (ic8-ic9-all)
# https://access.redhat.com/insights/planner/34921
# Generated by Red Hat Insights on Tue, 09 Apr 2019 13:59:14 GMT

# Warning: Some of the rules in the plan do not have Ansible support and this playbook does not address them!

- name: run insights to obtain latest report info
  hosts: ic8.example.com,ic9.example.com
  become: True
  tasks:
        - name: determine insights version
          shell: 'redhat-access-insights --version'
          changed_when: false
          register: insights_version
        - when: insights_version.stdout[0:2] != '1.'
          Block:
[...]

Output truncated

```
Now, simply proceed to remediate the issues by executing the Ansible
Playbook as follows:

```
[root@tower \~]\# ansible-playbook ic8-ic9-all.yml

[...]

Output truncated
```

Please note that when the execution is completed (this may take a
while), the Insights agent is also run, so the latest state of the
system is reporting into Insights automatically.

Note that some of the issues won’t be able to be solved by the ansible
playbook and you’d need to do some extra steps.

Now from the Satellite UI, click on Red Hat Insights → Inventory you
will notice that your systems have few issues.



Continue to the next step: [Lab 4: Introduction to Ansible Tower](../lab4-tower/index.md)
